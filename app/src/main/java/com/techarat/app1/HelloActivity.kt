package com.techarat.app1

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.techarat.app1.databinding.ActivityHelloBinding

class HelloActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHelloBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =ActivityHelloBinding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val name = intent?.extras?.getSerializable("name").toString()
        binding.ShowName.text = name
    }  override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}