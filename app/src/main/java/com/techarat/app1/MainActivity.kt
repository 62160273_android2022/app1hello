package com.techarat.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.techarat.app1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var listIntent: Intent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(R.layout.activity_main)
        setContentView(binding.root)
        // Launch the VerticalListActivity on verticalBtn click
        binding.helloBtn.setOnClickListener { launchHello() }
        findViewById<Button>(R.id.helloBtn).setOnClickListener{
            var intent =Intent(this,HelloActivity::class.java)
            var name = binding.Name.text.toString()
            Log.d("Showname",name)
            intent.putExtra("name",name)
            this.startActivity(intent)
        }


    }

    private fun launchHello() {
        listIntent = Intent(this, HelloActivity::class.java)
        startActivity(listIntent)
    }
}
